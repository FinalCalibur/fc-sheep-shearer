package scripts.fc.missions.fcsheepshearer.tasks;

import org.tribot.api2007.Inventory;
import org.tribot.api2007.Player;
import org.tribot.api2007.WebWalking;

import scripts.fc.api.interaction.impl.npcs.NpcDialogue;
import scripts.fc.framework.task.Task;
import scripts.fc.missions.fcsheepshearer.FCSheepShearer;

public class CompleteQuest extends Task
{
	private final int DISTANCE_THRESHOLD = 2;
	
	@Override
	public void execute()
	{
		if(Player.getPosition().distanceTo(FCSheepShearer.FARMER_TILE) > DISTANCE_THRESHOLD)
			WebWalking.walkTo(FCSheepShearer.FARMER_TILE);
		else
			new NpcDialogue("Talk-to", "Fred the Farmer", 10, 0).execute();
			
	}

	@Override
	public boolean shouldExecute()
	{
		return Inventory.getCount("Ball of wool") >= 20;
	}

	@Override
	public String getStatus()
	{
		return "Complete quest";
	}

}
