package scripts.fc.missions.fcsheepshearer.tasks;

import org.tribot.api.DynamicClicking;
import org.tribot.api.Timing;
import org.tribot.api.interfaces.Positionable;
import org.tribot.api2007.Camera;
import org.tribot.api2007.Game;
import org.tribot.api2007.Inventory;
import org.tribot.api2007.NPCs;
import org.tribot.api2007.Player;
import org.tribot.api2007.WebWalking;
import org.tribot.api2007.ext.Filters;
import org.tribot.api2007.types.RSArea;
import org.tribot.api2007.types.RSNPC;
import org.tribot.api2007.types.RSTile;

import scripts.fc.api.generic.FCConditions;
import scripts.fc.api.interaction.impl.objects.ClickObject;
import scripts.fc.framework.task.Task;
import scripts.fc.missions.fcsheepshearer.FCSheepShearer;
import scripts.fc.missions.fcsheepshearer.data.QuestStage;

public class ShearSheep extends Task
{
	public static final RSArea SHEEP_PEN = new RSArea(new RSTile[]{
			new RSTile(3193, 3277, 0), new RSTile(3211, 3277, 0),
			new RSTile(3212, 3257, 0), new RSTile(3193, 3257, 0)});
	
	private final Positionable STILE_TILE = new RSTile(3197, 3278, 0);
	
	@Override
	public void execute()
	{
		if(!SHEEP_PEN.contains(Player.getPosition()))
			enterPen();
		else
			shearSheep();
	}

	@Override
	public boolean shouldExecute()
	{
		return Game.getSetting(FCSheepShearer.QUEST_SETTING_INDEX) == QuestStage.STARTED.getSetting()
				&& Inventory.getCount("Wool") < 20 && Inventory.getCount("Ball of wool") == 0
				&& Inventory.getCount("Shears") > 0;
	}

	@Override
	public String getStatus()
	{
		return "Shear sheep";
	}
	
	private void shearSheep()
	{
		RSNPC[] npcs = NPCs.findNearest(Filters.NPCs.nameEquals("Sheep")
				.combine(Filters.NPCs.actionsContains("Shear"), false)
				.combine(Filters.NPCs.actionsNotContains("Talk-to"), false)
				.combine(Filters.NPCs.inArea(SHEEP_PEN), false));
		
		if(npcs.length > 0)
		{
			if(!npcs[0].isClickable())
				Camera.turnToTile(npcs[0]);
			else
				if(DynamicClicking.clickRSNPC(npcs[0], "Shear"))
					Timing.waitCondition(FCConditions.inventoryChanged(Inventory.getAll().length), 4000);
		}
	}
	
	private void enterPen()
	{
		if(Player.getPosition().distanceTo(STILE_TILE) > 3)
			WebWalking.walkTo(STILE_TILE);
		else
			if(new ClickObject("Climb-over", "Stile", 5).execute())
				Timing.waitCondition(FCConditions.inAreaCondition(SHEEP_PEN), 4000);
	}

}
