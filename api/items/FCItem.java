package scripts.fc.api.items;

import org.tribot.api2007.Banking;
import org.tribot.api2007.Inventory;

public class FCItem
{
	private String name;
	private int id;
	private int amt;
	private boolean isStackable;
	
	public FCItem(int id, int amt, boolean isStackable)
	{
		this.id = id;
		this.amt = amt;
		this.isStackable = isStackable;
	}
	
	public FCItem(String name, int amt, boolean isStackable)
	{
		this.name = name;
		this.amt = amt;
		this.isStackable = isStackable;
	}
	
	public int getId()
	{
		return id;
	}
	
	public int getAmt()
	{
		return amt;
	}
	
	public boolean isStackable()
	{
		return isStackable;
	}
	
	public String getName()
	{
		return name;
	}
	
	public boolean deposit(int amt)
	{
		return name == null ? Banking.deposit(amt, id) : Banking.deposit(amt, name);
	}
	
	public boolean withdraw(int amt)
	{
		return name == null ? Banking.withdraw(amt, id) : Banking.withdraw(amt, name);
	}
	
	public int getWithdrawAmt()
	{
		final int INV_COUNT = amt - getInvCount(isStackable);
		
		return INV_COUNT < 0 ? 0 : INV_COUNT;
	}
	
	public int getRequiredInvSpace()
	{
		return isStackable ? 1 : amt;
	}
	
	public int getInvCount(boolean stackIncluded)
	{
		final int COUNT = name == null ? Inventory.getCount(id) : Inventory.getCount(name);
		
		if(isStackable && !stackIncluded && COUNT > 1)
			return 1;
		
		return COUNT;
	}
}
